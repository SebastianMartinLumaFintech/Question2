### Task

1. go to `app/secondary/page.tsx`
1. run the application
1. Navigate to `http://localhost:3000`
1. navigate to `https://example-one-test.vercel.app/done/2` in a new tab to see expected result
1. Looking at the expected result, debug why the page is not rendering as expected.
